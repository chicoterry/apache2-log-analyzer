#!usr/bin/python
#UNAM-CERT

#	Autores:
#		Espinosa Curiel Oscar
#		Ferrusca Jorge
#		Rodriguez Gallardo Pedro

"""
	Importamos la funcion para abrir archivos comprimidos
	Importamos la clase para obtener parametros como banderas
"""
from gzip import open as gopen
from optparse import OptionParser


def addOptions():
	parser = OptionParser()
	parser.add_option('-I', '--input', dest='input', default=None, help='Files that will be analyzed. If there is more than one, they must be separated by commas')
	options, args = parser.parse_args()
	return options

def isGzipFile(binnacle):
	"""
		Funcion que valida si el agumento dado es o no un archivo comprimido
		gzfile. Se hace comparando el magic number de gzip con los primeros
		cuatro numeros hexadecimales del archivo.
	"""
	magic_header = '\x1f\x8b'
	#Abrimos el archivo en modo lectura binaria
	with open(binnacle, 'rb') as gFile:
		startFile = gFile.read(len(magic_header))
		if startFile == magic_header:
			return True

	return False

def splitInput(inputFiles):
	"""
		Funcion que separa en una lista los archivos dados como argumento
	"""
	return inputFiles.split(',')


if __name__ == '__main__':
	opts = addOptions()
	files = splitInput(opts.input)
	for file in files:
		print isGzipFile(file)
